package database.mongodb

import com.mongodb.casbah.Imports._
import models.UserModel.UserModel

import scala.collection.mutable

/**
  * Created by lrbpi on 17/01/2016.
  */
object User {

  val mongoClient = MongoClient("localhost", 27017)
  val db = mongoClient("Memoria")
  val coll = User.db("users")

  def findAll () : mutable.Set[UserModel] = {
    val result : mutable.Set[UserModel] = mutable.Set[UserModel]()
    val users = User.coll.find()
    for(user <- users) {
      result += UserModel(
        user.get("_id").toString,
        user.get("login").toString,
        user.get("password").toString,
        user.get("firstName").toString,
        user.get("lastName").toString,
        user.get("rank").toString
      )
    }
    result
  }

  def findUser (login : String, password : String) : Option[UserModel] = {
    var result : Option[UserModel] = None
    val q = MongoDBObject("login" -> login, "password" -> password )
    val cursor = User.coll.findOne(q)
    if (cursor.nonEmpty) {
      result = Some(UserModel(
        cursor.get("_id").toString,
        cursor.get.get("login").toString,
        cursor.get.get("password").toString,
        cursor.get.get("firstName").toString,
        cursor.get.get("lastName").toString,
        cursor.get.get("rank").toString
      ))
    }
    result
  }

  def createUser (userModel : UserModel) : Boolean = {
    val coll = User.db("users")
    val builder = MongoDBObject.newBuilder
    builder += "login" -> userModel.login
    builder += "password" -> userModel.password
    builder += "firstName" -> userModel.firstName
    builder += "lastName" -> userModel.lastName
    builder += "rank" -> userModel.rank

    val user = builder.result
    coll.insert(user)
    true
  }



}
