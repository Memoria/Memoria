package controllers

import javax.inject.{Inject, Singleton}

import org.slf4j.{Logger, LoggerFactory}
import play.api.libs.functional.syntax._
import play.api.libs.json._
import play.api.mvc._

/**
  * Created by Pierre on 14/07/2015.
  */



@Singleton
class Connection @Inject() extends Controller {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[Connection])


  implicit val rds = (
    (__ \ 'login).read[String] and  (__ \ 'password).read[String]) tupled




  def connect = Action(BodyParsers.parse.json) {

    request =>
      val json = request.body
     /* val future = UserService ? (json.\("login").as[String], json.\("password").as[String])
      val user   = Await.result(future, timeout.duration).asInstanceOf[Option[UserModel]]
      if (user.isEmpty) {
        Forbidden("Error")
      } else {
        Ok(user.get.toString)
      }*/

      Ok("Connecté")
  }






}


