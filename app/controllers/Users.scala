package controllers

import javax.inject.Singleton

import org.slf4j.{Logger, LoggerFactory}
import play.api.mvc._

@Singleton
class Users extends Controller {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[Users])

  /**
    * TODO
    * URL : /users
    * @return all the users in database
    */
  def getUsers = Action {
      Ok("To do")

    // First step : ask Redis cache

    // Second step : ask MongoDB
  }

  /**
    * TODO
    * URL : /users/idUser
    * @return user with the id taken in parameter
    */
  def getUser (idUser : String) = Action {
    Ok("tTo do")
  }

  /**
    *  TODO
    *  URL : /users
    * @return JSON with status of the request
    */
  def createUser = Action(BodyParsers.parse.json) {
    request => Ok("ok")
   /* request =>
      val user = request.body.validate[UserModel]
      user.fold(
        errors => {
          BadRequest(Json.obj("status" -> "Bad Request", "message" -> JsError.toFlatJson(errors)))
        },
        user => {
          val result : Boolean = UserService.createUser(user)
          if (result)
            Ok(Json.obj("status" -> "OK -> Success during insertion"))
          else
            Ok(Json.obj("status" -> "Error -> Error during insertion"))
        }
      )*/
  }

  /**
    * TODO
    * URL : /users/IDUser
    * @param IDUser
    * @return JSON with status of the request
    */
  def updateUser (IDUser : String) = Action(BodyParsers.parse.json) {
      request =>
      Ok("In progress ... !")
  }

  /**
    * TODO
    * URL : /users/IDUser
    * @param IDUser
    * @return JSON with status of the request
    */
  def deleteUSer(IDUser : String) = Action {
      Ok("In progress ... !")
  }

}
