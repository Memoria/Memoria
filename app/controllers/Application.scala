package controllers

import javax.inject.{Singleton, Inject}
import play.api.Play
import play.api.Play.current
import org.slf4j.{LoggerFactory, Logger}
import play.api.mvc._

@Singleton
class Application @Inject() extends Controller {

  private final val logger: Logger = LoggerFactory.getLogger(classOf[Application])

  def index = Action {
    logger.info("Serving index page...")
    val adminPageFile = Play.getFile("/public/index.html")
    Ok.sendFile(adminPageFile, inline = true)
  }
}
