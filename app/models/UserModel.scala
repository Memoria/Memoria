package models

import play.api.libs.json.Json



/**
  * Created by Pierre on 13/11/2015.
  */
object UserModel {

  case class UserModel (id : String,
                        login : String,
                        password : String,
                        firstName : String,
                        lastName : String,
                        rank : String
                        )

  implicit val userWrites = Json.writes[UserModel]
  implicit val userReads = Json.reads[UserModel]
}
